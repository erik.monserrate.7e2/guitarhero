﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorScript : MonoBehaviour
{
    Component halo;
    void OnTriggerEnter(Collider col)
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            halo.GetType().GetProperty("enabled").SetValue(halo, true, null);
        }
        Debug.Log(col.gameObject.name + " has collided with " + this.name);
    }

    private void OnTriggerExit(Collider other)
    {
        halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
    }
    // Start is called before the first frame update
    void Start()
    {
        halo = GetComponent("Halo");
       
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
